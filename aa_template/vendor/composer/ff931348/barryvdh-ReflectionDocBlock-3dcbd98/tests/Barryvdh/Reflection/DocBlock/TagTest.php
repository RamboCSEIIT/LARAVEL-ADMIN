<?php
/**
 * phpDocumentor
 *
 * PHP Version 5.3
 *
 * @author    Vasil Rangelov <boen.robot@gmail.com>
 * @copyright 2010-2011 Mike van Riel / Naenius (http://www.naenius.com)
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://phpdoc.org
 */

namespace Barryvdh\Reflection\DocBlock\Tag;

use Barryvdh\Reflection\DocBlock\Tag;

/**
 * Reflection class for a @version tag in a Docblock.
 *
 * @author  Vasil Rangelov <boen.robot@gmail.com>
 * @license http://www.opensource.org/licenses/mit-license.php MIT
 * @link    http://phpdoc.org
 */
class VersionTag extends Tag
{
    /**
     * PCRE regular expression matching a version vector.
     * Assumes the "x" modifier.
     */
    const REGEX_VECTOR = '(?:
        # Normal release vectors.
        \d\S*
        |
        # VCS version vectors. Per PHPCS, they are expected to
        # follow the form of the VCS name, followed by ":", followed
        # by the version vector itself.
        # By convention, popular VCSes like CVS, SVN and GIT use "$"
        # around the actual version vector.
        [^\s\:]+\:\s*\$[^\$]+\$
    )';

    /** @var string The version vector. */
    protected $version = '';
    
    public function getContent()
    {
        if (null === $this->content) {
            $this->content = "{$this->version} {$this->description}";
        }

        return $this->content;
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        parent::setContent($content);

        if (preg_match(
            '/^
                # The version vector
                (' . self::REGEX_VECTOR . ')
                \s*
                # The description
                (.+)?
            $/sux',
            $this->description,
            $matches
        )) {
            $this->version = $matches[1];
            $this->setDescription(isset($matches[2]) ? $matches[2] : '');
            $this->content = $content;
        }

        return $this;
    }

    /**
     * Gets the version section of the tag.
     *
     * @return string The version section of the tag.
     */
    public function getVersion()
    {
        return $this->version;
    }
    
    /**
     * Sets the version section of the tag.
     * 
     * @param string $version The new version section of the tag.
     *     An invalid value will set an empty string.
     * 
     * @return $this
     */
    public function setVersion($version)
    {
        $this->version
            = preg_match('/^' . self::REGEX_VECTOR . '$/ux', $version)
            ? $version
            : '';

        $this->content = null;
        return $this;
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  # Async-Limiter

A module for limiting concurrent asynchronous actions in flight. Forked from [queue](https://github.com/jessetane/queue).

[![npm](http://img.shields.io/npm/v/async-limiter.svg?style=flat-square)](http://www.npmjs.org/async-limiter)
[![tests](https://img.shields.io/travis/STRML/async-limiter.svg?style=flat-square&branch=master)](https://travis-ci.org/STRML/async-limiter)
[![coverage](https://img.shields.io/coveralls/STRML/async-limiter.svg?style=flat-square&branch=master)](https://coveralls.io/r/STRML/async-limiter)

This module exports a class `Limiter` that implements some of the `Array` API.
Pass async functions (ones that accept a callback or return a promise) to an instance's additive array methods.

## Motivation

Certain functions, like `zlib`, have [undesirable behavior](https://github.com/nodejs/node/issues/8871#issuecomment-250915913) when
run at infinite concurrency.

In this case, it is actually faster, and takes far less memory, to limit concurrency.

This module should do the absolute minimum work necessary to queue up functions. PRs are welcome that would
make this module faster or lighter, but new functionality is not desired.

Style should confirm to nodejs/node style.

## Example

``` javascript
var Limiter = require('async-limiter')

var t = new Limiter({concurrency: 2});
var results = []

// add jobs using the familiar Array API
t.push(function (cb) {
  results.push('two')
  cb()
})

t.push(
  function (cb) {
    results.push('four')
    cb()
  },
  function (cb) {
    results.push('five')
    cb()
  }
)

t.unshift(function (cb) {
  results.push('one')
  cb()
})

t.splice(2, 0, function (cb) {
  results.push('three')
  cb()
})

// Jobs run automatically. If you want a callback when all are done,
// call 'onDone()'.
t.onDone(function () {
  console.log('all done:', results)
})
```

## Zlib Example

```js
const zlib = require('zlib');
const Limiter = require('async-limiter');

const message = {some: "data"};
const payload = new Buffer(JSON.stringify(message));

// Try with different concurrency values to see how this actually
// slows significantly with higher concurrency!
//
// 5:        1398.607ms
// 10:       1375.668ms
// Infinity: 4423.300ms
//
const t = new Limiter({concurrency: 5});
function deflate(payload, cb) {
  t.push(function(done) {
    zlib.deflate(payload, function(err, buffer) {
      done();
      cb(err, buffer);
    });
  });
}

console.time('deflate');
for(let i = 0; i < 30000; ++i) {
  deflate(payload, function (err, buffer) {});
}
q.onDone(function() {
  console.timeEnd('deflate');
});
```

## Install

`npm install async-limiter`

## Test

`npm test`

## API

### `var t = new Limiter([opts])`
Constructor. `opts` may contain inital values for:
* `q.concurrency`

## Instance methods

### `q.onDone(fn)`
`fn` will be called once and only once, when the queue is empty.

## Instance methods mixed in from `Array`
Mozilla has docs on how these methods work [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array).
### `q.push(element1, ..., elementN)`
### `q.unshift(element1, ..., elementN)`
### `q.splice(index , howMany[, element1[, ...[, elementN]]])`

## Properties
### `q.concurrency`
Max number of jobs the queue should process concurrently, defaults to `Infinity`.

### `q.length`
Jobs pending + jobs to process (readonly).

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  x��ks��&���+·�xg"�L��w�K����&�6)�lJ�:�(vɲ�U}��%����Ľ�H ю�i�j<σD�$�������������?�����e�䇾��|����{����������6��/�o��5���+�?��|/�]Ӟ��/���]