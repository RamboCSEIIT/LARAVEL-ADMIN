var SCRIPTS_OUT_PATH                                                = 'aa_template/public/02_SCRIPTS';
var SCRIPTS_IN_PATH                                    = '02_SCRIPTS/AA_LOGIN/**/*.*';
var SCRIPTS_OUT_FILE_NAME                                               = 'aa_login.js';


var SASS_OUT_PATH                                                       = 'aa_template/public/01_CSS';
var SASS_IN_PATH                                      = '01_SASS/AA_LOGIN/style.scss';
var SASS_OUT_FILE_NAME                                                 = 'aa_login.css';

var VIEW_T_IN_FRAG                         = '03_ViewsT/ab_Fragments/AA_LOGIN/**/*.*';
var VIEW_T_OUT_BODY                      = '03_ViewsT/aa_WorkSpace/zz_body/AA_LOGIN';
 

module.exports = 
{
  
  aa_login_mode : function ()
  {
  
                var options_script = {
                    SCRIPTS_OUT_PATH: SCRIPTS_OUT_PATH,
                    SCRIPTS_IN_PATH: SCRIPTS_IN_PATH,
                    SCRIPTS_OUT_FILE_NAME: SCRIPTS_OUT_FILE_NAME
                    
                };



                var options_sass = {
                    SASS_OUT_PATH: SASS_OUT_PATH,
                    SASS_IN_PATH: SASS_IN_PATH,
                    SASS_OUT_FILE_NAME: SASS_OUT_FILE_NAME
                    
                };


                var options_body = {
                    VIEW_T_OUT_BODY: VIEW_T_OUT_BODY,
                    VIEW_T_IN_FRAG: VIEW_T_IN_FRAG
                    
                };

                   return (javaScript(options_script) && SCSS(options_sass) && BODY_HTML(options_body) );

 

      
  },
 get_function: function ()
 {
     return this.aa_login_mode ;
 }




};