<!DOCTYPE html>
<html lang=en>

<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta http-equiv=ScreenOrientation content=autoRotate:disabled>
    <meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name=description content="">
    <meta name=author content="">
    <link rel="shortcut icon" type=image/png href=02_IMAGES/favicon.png>
    <link rel="stylesheet" href="01_CSS_MAT/aa_LOGIN/style.min.css" type="text/css">
    @yield('cssBlock')
    <title>@yield('title')</title>
</head>

<body id=body data-spy=scroll data-offset=1>
@yield('content')
<script src=https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js></script>
<script src="01_CSS_MAT/aa_LOGIN/libs/popper.js/dist/umd/popper.min.js"></script>
<script src= "01_CSS_MAT/aa_LOGIN/libs/bootstrap/dist/js/bootstrap.min.js"></script>

@yield('bottomJS')
</body>

</html>